%% МИС
%% втор колоквиум
%% Ојлерова постапка
%% пример II-2
clc
clear
T = 0.05;
t(1) = 1;
x(1) = 3;
f = @(x,t) x^2*t;
for k = 1:1:6
    x(k+1) = x(k) + T*f(x(k),t(k));
    t(k+1) = t(k)+T;
end
disp(0:1:6)
disp(t)
disp(t+0.05)
disp(x)
tv = 1:0.05:1.30;
xv = 6./(5-3*tv.^2);
disp(xv)
% plot(t,x,'r',tv,xv,'b')
plot(t,x)
hold on
plot(tv,xv)
axis([1 1.30 0 20])
legend(['Ојлерова'], ['Вистинска'])
%% пример II-3
clc
clear
k1 = 0;
y = 0;
f = @(x,t) (k1*y - x)/(1+x);
x(1) = 2;
t(1) = 0;
T = 0.2;
for k = 1:1:10
    x(k+1) = x(k)+T*f(x(k),t(k));
    x_delta(k) = T*f(x(k),t(k));
    t(k+1) = t(k)+T;
end
x_delta(11) = T*f(x(11),t(11));
disp(0:10)
disp(0:0.2:2)
disp(x)
disp(x_delta)
%% Усовршена Ојлерова постапка
%% пример II-4
clc
clear
T = 0.2;
x(1) = 1;
t(1) = 0;
f = @(x,t) x-2*t/x;

for k = 1:5
    x(k+1) = x(k) + T*f(x(k),t(k));
    t(k+1) = t(k) + T;
end
disp(t)
disp(x)
%% Тајлорова постапка
%% пример II-5
clc
clear all
f = @(x,t) (x^2)*t;
Tf = 1.30;
T = 0.05;
t(1) = 1;
x(1) = 3;
for k = 1:6
    x(k+1) = x(k)+T*f(x(k),t(k))+1/2*T^2*((x(k))^2+f(x(k),t(k))*x(k)*t(k)*2);
    t(k+1) = t(k)+T;
end
disp(x)
%% Рунге-Кута постапка
%% пример II-6
clc
clear
x(1)=3;
T0=1; 
Tf=1.3; 
t(1)=1;
T=0.05;
n=floor((Tf-T0)/T); 
f=@(x,t) t*(x^2); 
for k=1:n
 K1=f(x(k),t(k));
 K2=f(x(k)+(1/2)*T*K1,t(k)+(1/2)*T);
 K3=f(x(k)+(1/2)*T*K2,t(k)+(1/2)*T);
 K4=f(x(k)+T*K3,t(k)+T);
 x(k+1)=x(k)+(1/6)*T*(K1+2*K2+2*K3+K4);
 t(k+1)=t(k)+T;
end
disp(x(7))
%% Моделирање и дигитална симулација на системи од повисок ред
%% пример II-7 со Ојлерова постапка
clc
clear all
f = @(x,y,t) t - 3*x*y;
t(1) = 0;
x(1) = 2;
y(1) = 1;
T = 0.05;
for k = 1:15
    t(k+1) = t(k)+T;
    x(k+1) = x(k) + T*y(k);
    y(k+1) = y(k) + T*(t(k)-3*x(k)*y(k));
end
disp(x)
disp(y)
%% пример II-7 со Тајлорова формула
clc
clear all
f = @(x,y,t) t - 3*x*y;
t(1) = 0;
x(1) = 2;
y(1) = 1;
T = 0.05;
for k = 1:10
    K11 = y(k);
    K21 = t(k)-3*x(k)*y(k);
    K12 = y(k)+1/2*T*K21;
    K22 = t(k)+1/2*T-3*(x(k)+1/2*T*K11)*(y(k)+1/2*T*K21);
    K13 = y(k)+1/2*T*K22;
    K23 = t(k)+1/2*T-3*(x(k)+1/2*T*K12)*(y(k)+1/2*T*K22);
    K14 = y(k)+1/2*T*K23;
    K24 = t(k)+T-3*(x(k)+T*K13)*(y(k)+T*K23);
    x(k+1) = x(k)+1/6*T*(K11+2*(K12+K13)+K14);
    y(k+1) = y(k)+1/6*T*(K21+2*(K22+K23)+K24);
    t(k+1) = t(k)+T;
end
disp(x)
disp(y)
%% Моделирање и симулација на нелинеарни динамички системи
%% пример II-8
clc
clear all
fx = @(x,y,t) 50*x*(1-0.02*y);
fy = @(x,y,t) 10*y*(-1+0.05*x);
x(1) = 10;
y(1) = 50;
t(1) = 0;
T = 0.00025;
n=floor((5-0)/T)
for k = 1:n
    x(k+1) = x(k) + T*fx(x(k),y(k),t(k));
    y(k+1) = y(k) + T*fy(x(k),y(k),t(k));
    t(k+1) = t(k)+T;
end
figure(1)
plot(t,x,t,y)
figure(2)
plot(x,y)
%% Бетка збирка код
clc
clear all
x0=10; y0=50; t0=0; tn=5; h = 0.00025;
t=t0; x=x0; y = y0;
X(1) = x0; Y(1) = y0; T(1) = t0;
for k = 2:20000
    x1 = x*(1+50*h-50*h*0.02*y);
    y1 = y*(1-10*h+10*h*0.050*x);
    x=x1;
    y=y1;
    t = t + h;
    T(k) = t;
    X(k) = x;
    Y(k) = y;
end
figure(3)
plot(T,X,T,Y)
figure(4)
plot(X,Y)
%% пример II-9
clc
clear all
fx = @(x,y,z,t) 2*x*(1-0.1*y+0.05*z);
fy = @(x,y,z,t) y*(1+0.05*x-0.02*z);
fz = @(x,y,z,t) 10*z*(1-0.04*x+0.01*y);
T = 0.0001;
n = floor((2-0)/T);
x(1)=5;
y(1)=10;
z(1)=8;
t(1)=0;
for k = 1:n
   x(k+1) = x(k) + T*fx(x(k),y(k),z(k),t(k));
   x_delta(k+1) = fx(x(k),y(k),z(k),t(k));
   y(k+1) = y(k) + T*fy(x(k),y(k),z(k),t(k));
   y_delta(k+1) = fy(x(k),y(k),z(k),t(k));
   z(k+1) = z(k) + T*fz(x(k),y(k),z(k),t(k));
   z_delta(k+1) = fz(x(k),y(k),z(k),t(k));
   t(k+1) = t(k)+T; 
end
figure(1)
plot(t,x,t,y,t,z)
figure(2)
plot(x,x_delta)
figure(3)
plot(y,y_delta)
figure(4)
plot(z,z_delta)  
%% пример III - 1
clc
clear all
syms D1 D2 D3
x = [0 0.1548 0.3996 0.6035];
x_ = 1 - x;
rav1 = vpa(x_(1)+x_(2)*D1+x_(3)*D2,2);
rav2 = x_(2)+x_(3)*D1+x_(4)*D2;
r = solve([rav1 rav2],[D1 D2]);
rD1 = vpa(r.D1,5);
rD2 = vpa(r.D2,5);
syms z
az = [rD2 rD1 1];
koreni = roots(az);
T=0.5;
s1 = -1/T*log(koreni(1))
s2 = -1/T*log(koreni(2))
%% пример III - 2
clc
clear all
syms D1 D2 D3
x = [0 0.283 0.772 1.1085];
x_ = 1 - x;
rav1 = vpa(x_(1)+x_(2)*D1+x_(3)*D2,2);
rav2 = x_(2)+x_(3)*D1+x_(4)*D2;
r = solve([rav1 rav2],[D1 D2]);
rD1 = vpa(r.D1,5);
rD2 = vpa(r.D2,5);
syms z
az = [rD2 rD1 1];
koreni = roots(az);
T=0.5;
s1 = -1/T*log(koreni(1))
s2 = -1/T*log(koreni(2))
%% II-1.4
x(1) = 0;
t(1) = 0;
T = 0.1;
n = floor((0.5-0)/T);
f = @(x,t) t-x;
for k = 1:n
    x(k+1) = x(k)+T*f(x(k),t(k));
    t(k+1) = t(k)+T;
end
disp(x)
%% II-1.5
clc
clear all
x(1) = 1;
t(1) = 0;
T = 0.1;
n = floor((1-0)/T);
f = @(x,t) t/2*x;
for k = 1:n
    x(k+1) = x(k)+T*f(x(k),t(k));
    t(k+1) = t(k)+T;
end
disp(x)
%% II-1.6
clc
clear all
x(1) = 1;
t(1) = 0;
T = 0.1;
n = floor((0.5-0)/T);
f = @(x,t) t+x;
for k = 1:n
    x(k+1) = x(k)+T*f(x(k),t(k));
    t(k+1) = t(k)+T;
end
disp(x)
%% II-1.7
clc
clear all
x(1) = 1;
t(1) = 1;
T = 0.2;
n = floor((2-1)/T);
f = @(x,t) t+x^2;
for k = 1:n
    x(k+1) = x(k)+T*f(x(k),t(k));
    t(k+1) = t(k)+T;
end
disp(x)
%% II-1.8
clc
clear all
x(1) = 1;
t(1) = 0;
T = 0.1;
n = floor((0.5-0)/T);
f = @(x,t) (2*t+x^2)/x;
for k = 1:n
    x(k+1) = x(k)+T*f(x(k),t(k));
    t(k+1) = t(k)+T;
end
disp(x)
%% II-1.9
clc
clear all
x(1) = 2;
t(1) = 0;
T = 0.2;
n = 12;
f = @(x,t) -x/(1+abs(x));
for k = 1:n
    x(k+1) = x(k)+T*f(x(k),t(k));
    t(k+1) = t(k)+T;
end
disp(x)
%% II-1.10
clc
clear all
v1(1) = 0;
v2(1) = 1;
fv1 = @(v1, v2, t) v2;
fv2 = @(v1, v2, t) -4*v1-5*v2;
t(1) = 0;
n = floor((0.2-0)/0.05);
T = 0.05;
for k=1:n
    v1(k+1) = v1(k)+T*fv1(v1(k),v2(k),t(k));
    v2(k+1) = v2(k)+T*fv2(v1(k),v2(k),t(k));
    t(k+1) = t(k)+T;
end
disp(v1)
disp(v2)
%% II-1.11
clc
clear all
v1(1) = 0;
v2(1) = 1;
fv1 = @(v1, v2, t) v2;
fv2 = @(v1, v2, t) cos(v1+v2);
t(1) = 0;
T = 0.02;
for k=1:5
    v1(k+1) = v1(k)+T*fv1(v1(k),v2(k),t(k));
    v2(k+1) = v2(k)+T*fv2(v1(k),v2(k),t(k));
    t(k+1) = t(k)+T;
end
disp(v1)
disp(v2)
%% II-1.12
clc
clear all
v1(1) = 0;
v2(1) = 1;
fv1 = @(v1, v2, t) v2;
fv2 = @(v1, v2, t) -4*v1-5*v2;
t(1) = 0;
n = floor((0.2-0)/0.05);
T = 0.05;
for k=1:n
    v1(k+1) = v1(k)+T*fv1(v1(k),v2(k),t(k));
    v2(k+1) = v2(k)+T*fv2(v1(k),v2(k),t(k));
    t(k+1) = t(k)+T;
end
disp(v1)
disp(v2)
%% II-16
clc
clear all
v1(1) = 0;
v2(1) = 0;
v3(1) = 0;
y(1) = 0;

fv1 = @(v1, v2,v3 ,t) 6*v1+v2;
fv2 = @(v1, v2,v3 ,t) -11*v1+v3+1;
fv3 = @(v1, v2,v3, t) 6*v1+4;
t(1) = 0;
T = 0.01;
for k=1:5
    v1(k+1) = v1(k)+T*fv1(v1(k),v2(k),v3(k),t(k));
    v2(k+1) = v2(k)+T*fv2(v1(k),v2(k),v3(k),t(k));
    v3(k+1) = v3(k)+T*fv3(v1(k),v2(k),v3(k),t(k));
    t(k+1) = t(k)+T;
end
disp(v1)
disp(v2)
disp(v3)
%% II-1.17
clc
clear all
x(1)=2; y(1)=0;
fx = @(x,y,t) (y-x)/(1+abs(x));
fy = @(x,y,t) -10*x;
T = 0.1;
t(1)=0;
for k=1:30
    x(k+1)=x(k)+T*fx(x(k),y(k),t(k));
    y(k+1)=y(k)-x(k);
    t(k+1)=t(k)+T;
end
disp(x)
disp(y)
%% Тајлорова формула
%% II-2.1
syms x(t) y(t) t
xf = x+y+5*t
diff(xf, t)
yf = x^2+y^2+t^2
diff(yf, t)
%% II-2.2

syms x(t) y(t) t
xf = x*y+4*t;
yf = x+3*y-cos(t);
diff(xf,t)
diff(yf, t)
%% II-2.3
syms x1(t) x2(t) t
x2f = -3*x1*x2-x1+cos(t)
diff(x2f, t)
%% II-3.3
clc
clear all

x(1) = 1;
fx = @(x,t) t^2-exp(x)*sin(t);
t(1) = 0;
tf = 3;
T = 0.25
n = floor(3/T)
for k = 1:n-1
    K1 = fx(x(k),t(k));
    K2 = fx(x(k)+T/2*K1,t(k)+T/2*K1);
    K3 = fx(x(k)+T/2*K2,t(k)+T/2*K2);
    K4 = fx(x(k)+T*K3,t(k)+T*K3);
    x(k+1) = x(k)+T/6*(K1+2*(K2+K3)+K4);
    t(k+1) = t(k)+T;
end
plot(t,x)
hold on 
grid on
axis([0 2.5 0.4 1.8])
%% II-3.4
clc
clear all
fx = @(x,y,t) t^2*sin(x)+exp(t)*cos(y);
fy = @(x,y,t) 2*t*x+exp(y);
x(1) = 1;
y(1) = -1;
t(1) = 0;
n = 1/0.1;
T = 0.1;
for k = 1:n-1
    K11 = fx(x(k),y(k),t(k));
    K21 = fy(x(k),y(k),t(k));
    K12 = fx(x(k)+T/2*K11,y(k)+T/2*K21,t(k)+T/2);
    K22 = fy(x(k)+T/2*K11,y(k)+T/2*K21,t(k)+T/2);
    K13 = fx(x(k)+T/2*K12,y(k)+T/2*K22,t(k)+T/2);
    K23 = fy(x(k)+T/2*K12,y(k)+T/2*K22,t(k)+T/2);
    K14 = fx(x(k)+T*K13,y(k)+T*K23,t(k)+T);
    K24 = fy(x(k)+T*K13,y(k)+T*K23,t(k)+T);
    x(k+1) = x(k)+T/6*(K11+2*K12+2*K13+K14);
    y(k+1) = y(k)+T/6*(K21+2*K22+2*K23+K24);
    t(k+1) = t(k)+T;
end
plot(t,x,t,y)
%% II-3.5
clc
clear all

fx = @(x,y,t) x*(6-2*x-y);
fy = @(x,y,t) y*(4-x-y);
x(1) = 5;
y(1) = 10;
t(1) = 0;
n = 25/0.01
T = 0.01;
for k = 1:n-1
    K11 = fx(x(k),y(k),t(k));
    K21 = fy(x(k),y(k),t(k));
    K12 = fx(x(k)+T/2*K11,y(k)+T/2*K21,t(k)+T/2);
    K22 = fy(x(k)+T/2*K11,y(k)+T/2*K21,t(k)+T/2);
    K13 = fx(x(k)+T/2*K12,y(k)+T/2*K22,t(k)+T/2);
    K23 = fy(x(k)+T/2*K12,y(k)+T/2*K22,t(k)+T/2);
    K14 = fx(x(k)+T*K13,y(k)+T*K23,t(k)+T);
    K24 = fy(x(k)+T*K13,y(k)+T*K23,t(k)+T);
    x(k+1) = x(k)+T/6*(K11+2*K12+2*K13+K14);
    y(k+1) = y(k)+T/6*(K21+2*K22+2*K23+K24);
    t(k+1) = t(k)+T;
end
% size(x)
% size(y)
% size(t)
plot(t,x,'r',t,y,'g')
axis([0 25 0 5])
%% II-4.1
%% III-2
clc
clear all
syms x
r = solve(sin(pi/3*x)-x,x)
